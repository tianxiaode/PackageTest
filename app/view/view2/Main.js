Ext.define('PackageTest.view.view2.Main',{
    extend:"Ext.panel.Panel",
    xtype: 'view2',
    viewModel: 'view2',    

    tbar:[
        { iconCls: 'x-fa fa-file-o', bind:{ hidden: '{!create}' } },
        { iconCls: 'x-fa fa-pencil', bind:{ hidden: '{!update}' } },
        { iconCls: 'x-fa fa-trash', bind:{ hidden: '{!delete}' } }
    ]

})