Ext.define('PackageTest.view.view2.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.view2',

    data: {
    },

    formulas: {
        create: function (get) {
            return ACL.checkPermission('createView2');
        },
        update: function (get) {
            return ACL.checkPermission('updateView2');
        },
        delete: function (get) {
            return ACL.checkPermission('deleteView2');
        }
    }
});