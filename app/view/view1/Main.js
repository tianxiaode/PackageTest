Ext.define('PackageTest.view.view1.Main',{
    extend:"Ext.panel.Panel",
    xtype: 'view1',
    viewModel: 'view1',    

    requires:[
        'Ext.form.FormPanel'
    ],

    tbar:[
        { iconCls: 'x-fa fa-file-o', bind:{ hidden: '{!create}' } },
        { iconCls: 'x-fa fa-pencil', bind:{ hidden: '{!update}' } },
        { iconCls: 'x-fa fa-trash', bind:{ hidden: '{!delete}' } }
    ],

    layout:'fit',

    items:[
        {
            xtype: 'form',
            defaults:{
                entityName: 'login'
            },
            items:[
                { xtype: 'textfield', name: 'name'},
                { xtype: 'textfield', name: 'password', inputType: 'password'},
                { xtype: 'checkboxfield', name: 'remberMe'}
            ]
        }
    ]

})