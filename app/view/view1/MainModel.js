Ext.define('PackageTest.view.view1.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.view1',

    data: {
    },

    formulas: {
        create: function (get) {
            return ACL.checkPermission('createView1');
        },
        update: function (get) {
            return ACL.checkPermission('updateView1');
        },
        delete: function (get) {
            return ACL.checkPermission('deleteView1');
        }
    }
});