﻿Ext.define('Common.overrides.form.field.Base', {
    override: 'Ext.form.field.Base',

    entityName: null,

    initComponent: function () {
        var me = this,
            entity= me.entityName,
            name = me.name,
            label;
        if (entity) {
            label = I18N['model'][entity][name];
            if(me.isCheckbox || me.isRadio){
                me.boxLabel = label;
            }else{
                me.fieldLabel = label;
            }
        }
        me.callParent();

        me.subTplData = me.subTplData || {};

        // Init mixins 
        me.initLabelable();
        me.initField();
        me.initDefaultName();

        // Add to protoEl before render 
        if (me.readOnly) {
            me.addCls(me.readOnlyCls);
        }

        me.addCls(Ext.baseCSSPrefix + 'form-type-' + me.inputType);

        // formatText is superseded by ariaHelp but we still apply it for compatibility 
        if (me.format && me.formatText && !me.ariaHelp) {
            me.ariaHelp = Ext.String.format(me.formatText, me.format);
        }
    }

});
