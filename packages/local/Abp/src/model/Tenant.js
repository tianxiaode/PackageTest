Ext.define('Abp.model.Tenant', {
    extend: 'Abp.model.Base',

    fields: [
        { name: 'name', defaultValue: '' },
        { name: 'tenancyName', defaultValue: '' },
        { name: 'isActive', type: 'bool', defaultValue: true }
    ]
        
})