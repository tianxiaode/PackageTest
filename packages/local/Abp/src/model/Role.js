Ext.define('Abp.model.Role', {
    extend: 'Abp.model.Base',

    fields: [
        { name: 'name', defaultValue: '' },
        { name: 'displayName', defaultValue: '' },
        { name: 'normalizedName', defaultValue: '' },
        { name: 'description', defaultValue: '' },
        { name: 'isStatic', type: 'bool', defaultValue: true }
        //{ name: 'permissions', type: 'auto'}
    ],
    hasMany: 'Permission'
        
})