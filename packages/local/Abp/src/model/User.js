﻿Ext.define('Abp.model.User', {
    extend: 'Abp.model.Base',

    fields: [
        { name: 'userName', defaultValue: '' },
        { name: 'name', defaultValue: '' },
        { name: 'surname', defaultValue: '' },
        { name: 'fullName', defaultValue: '' },
        { name: 'emailAddress', defaultValue: '' },
        { name: 'creationTime', type: 'date', dateFormat: I18N.DateTimeIsoFormat },
        { name: 'lastLoginTime', type: 'date', dateFormat: I18N.DateTimeIsoFormat },
        { name: 'isActive', type: 'bool', defaultValue: true },
        { name: 'roleNames', type: 'auto'}
    ]
        
})