Ext.define('Abp.model.Permission', {
    extend: 'Abp.model.Base',

    fields: [
        { name: 'name', defaultValue: '' },
        { name: 'displayName', defaultValue: '' },
        { name: 'description', defaultValue: '' }
    ]
        
})